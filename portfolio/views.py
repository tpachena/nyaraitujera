
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.shortcuts import render_to_response, redirect, get_object_or_404
import requests
from django.conf import settings
from django.template import RequestContext
from django.http import HttpResponse


def index(request):
    return render(request,'portfolio/index.html', {})

def home(request):
    return render(request,'portfolio/home.html', {})

def publications(request):
    return render(request,'portfolio/publications.html', {})

def achievements_rewards(request):
    return render(request,'portfolio/achievements_rewards.html', {})

def community_service(request):
    return render(request,'portfolio/community_service.html', {})

def gallery(request):
    return render(request,'portfolio/gallery.html', {})

def contact(request):
    return render(request,'portfolio/contact.html', {})


