from django.conf.urls import url

from portfolio import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^publications/$', views.publications, name='publications'),
    url(r'^achievements_rewards/$', views.achievements_rewards, name='achievements_rewards'),
    url(r'^community_service/$', views.community_service, name='community_service'),
    url(r'^gallery/$', views.gallery, name='gallery'),
    url(r'^contact/$', views.contact, name='contact'),
]