
"""
The nyarai_web project local settings.
"""

import os
from datetime import time

import environ
from django.utils.translation import ugettext_lazy as _


env = environ.Env()

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
DEBUG = 'DJANGO_DEBUG' in os.environ

SITE_URL = env.str('NYARAITUNJERA_SITE_URL', default='http://local.nyaraitunjera.com')
NYARAITUNJERA_ENV = env.str('NYARAITUNJERA_ENV', default='local')


ALLOWED_HOSTS = env.list('NYARAITUNJERA_ALLOWED_HOSTS', default=['localapi.nyaraitunjera.com', 'local.nyaraitunjera.com'])

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str('NYARAITUNJERA_SECRET_KEY', default=')uhxp+z$_*+f9lt@@7*c@h^wfpm*^yo-66jpk7go$9w_611gwn')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
DEBUG = os.environ.get('DJANGO_DEBUG', '') != 'False'

# Database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'nyaraidatabase',
    }
}

# Localization settings
LOCALE = 'en_ZA.UTF-8'

LANGUAGE_CODE = 'en-ZA'
TIME_ZONE = 'Africa/Johannesburg'