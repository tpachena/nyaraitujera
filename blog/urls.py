from django.conf.urls import url
from blog import views

urlpatterns = [
    url(r'^$', views.blog_index, name='blog_index'),
    url(r'^post/$', views.blog_index, name='blog_index'),
    url(r'^view-post/(?P<pk>[^\.]+)$', views.view_post, name='view_post'),
    url(r'^category/(?P<pk>[^\.]+)$', views.view_category, name='view_category'),
    url(r'^new-post/$', views.new_post, name='new_post'),
]