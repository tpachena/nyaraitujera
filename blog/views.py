# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render_to_response, get_object_or_404
from django.shortcuts import render, redirect
import requests
from blog.models import Post, Category, Comment
from .forms import CommentForm, PostForm
from django.contrib import messages
from django.template import RequestContext

 
def blog_index(request):
     posts = Post.objects.all().order_by('-created_on')
     context = {
         "posts": posts,
     }
     return render(request, "blog/blog_index.html", context)
    
def view_category(request, pk):
   category = get_object_or_404(Category, pk=pk)
   return render_to_response('blog/view_category.html', {
       'category': category,
       'posts': Post.objects.filter(category=category)[:5]
   })


def view_post(request, pk):
    post = Post.objects.get(pk=pk)
    form = CommentForm()
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = Comment(
                author=form.cleaned_data["author"],
                body=form.cleaned_data["body"],
                post=post
            )
            comment.save()
    comments = Comment.objects.filter(post=post)   
    return render(request, 'blog/comment_post.html', {
       'post': get_object_or_404(Post, pk=pk),
       'comments': comments,
       'form': form,
   })

def new_post(request):
    template = 'blog/new_post.html'
    form = PostForm(request.POST or None)

    try:
        if form.is_valid():
            form.save()
            messages.success(request, 'Your blog Post has been saved')
    except Exception as e:
        messages.warning(request, 'Your blog Post has not been saved to do an eror {}'.format(e))
    
    context = {
        'form': form,
    }
    return render(request, template, context)
