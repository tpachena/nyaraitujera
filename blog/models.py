# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

CATEGORY_CHOICES = (
    ('Achievements and Rewards', _('Achievements and Rewards')),
    ('Community Service', _('Community Service')),
    ('Contact', _('Contact')),
    ('Gallery', _('Gallery')),
    ('Publications', _('Publications')),
)

# Create your models here.
class Category(models.Model):
     name = models.CharField(
        choices=CATEGORY_CHOICES,
        verbose_name=_('Category'),
        max_length=30,
        blank=True, null=True
    )

     def __unicode__(self):
       return '%s' % self.name
 
class Post(models.Model):
     title = models.CharField(max_length=255)
     body = models.TextField()
     created_on = models.DateTimeField(auto_now_add=True)
     last_modified = models.DateTimeField(auto_now=True)
     category = models.ForeignKey('blog.Category', null=True)


class Comment(models.Model):
     author = models.CharField(max_length=60)
     body = models.TextField()
     created_on = models.DateTimeField(auto_now_add=True)
     post = models.ForeignKey('Post', on_delete=models.CASCADE)
