from django import forms
from blog.models import Post, Comment


class PostForm(forms.ModelForm):
    title = forms.CharField(widget=forms.Textarea(attrs={'cols': 60, 'rows': 5, "placeholder": "add tittle of your post here!"}))
    body = forms.CharField(widget=forms.Textarea(
        attrs={
            "class": "form-control",
            "placeholder": "add your post here!"
        })
    )
    class Meta:
        model = Post
        exclude = ['']

class CommentForm(forms.ModelForm):
    author = forms.CharField(
        max_length=60,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": "Type Your Name"
        })
    )
    body = forms.CharField(widget=forms.Textarea(
        attrs={
            "class": "form-control",
            "placeholder": "Leave a comment here!"
        })
    )

    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Comment
        fields = [
            'author', 'body'
        ]